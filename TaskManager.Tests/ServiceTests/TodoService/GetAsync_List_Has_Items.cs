﻿using Moq;
using NUnit.Framework;
using SpecsFor.StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskManager.Common.Contracts.ToDo;
using TaskManager.Data.Models;
using TaskManager.Data.Repositories;
using TaskManager.Services.Services;

namespace TaskManager.Tests.ServiceTests.TodoService
{
    [TestFixture]
    public class GetAsync_List_Has_Items : SpecsFor<ToDoService>
    {
        private readonly IToDoService _sut;
        private readonly Mock<IToDoRepository> _todoRepo = new Mock<IToDoRepository>();
        private int _todoId = 0;
        private IEnumerable<ToDoDto> _todos;

        public GetAsync_List_Has_Items()
        {
            _sut = new ToDoService(_todoRepo.Object);
        }

        protected override void Given()
        {
            base.Given();
            var rng = new Random();
            _todoId = rng.Next(1, 100);

            IEnumerable<ToDo> todos = new List<ToDo>()
            {
                new ToDo
                {
                    Id = _todoId,
                    DueDate = DateTime.MaxValue,
                    Description = Guid.NewGuid().ToString(),
                    Notes = Guid.NewGuid().ToString(),
                    Title = Guid.NewGuid().ToString()
                }
            };

            _todoRepo.Setup(x => x.GetAsync()).Returns(Task.FromResult(todos));
        }

        protected override void When()
        {
            base.When();
            _todos = _sut.GetAsync().Result;
        }

        [Test]
        public void Then_The_Todo_Repo_Is_Called_Once()
        {
            _todoRepo.Verify(x => x.GetAsync(), Times.Once);
            _todoRepo.VerifyNoOtherCalls();
        }

        [Test]
        public void Then_An_IEnumerable_Is_Returned()
        {
            Assert.NotNull(_todos);
        }

        [Test]
        public void Then_IEnumerable_Has_Items()
        {
            Assert.Greater(_todos.Count(), 0);
        }
    }
}
