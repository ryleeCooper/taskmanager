﻿using Moq;
using NUnit.Framework;
using SpecsFor.StructureMap;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TaskManager.Common.Contracts.ToDo;
using TaskManager.Data.Models;
using TaskManager.Data.Repositories;
using TaskManager.Services.Services;

namespace TaskManager.Tests.ServiceTests.TodoService
{
    [TestFixture]
    public class GetByIdAsync_Item_Does_Not_Exist : SpecsFor<ToDoService>
    {
        private readonly ToDoService _sut;
        private readonly Mock<IToDoRepository> _todoRepo = new Mock<IToDoRepository>();
        private int _todoId = 0;
        private ToDoDto _todoItem;

        public GetByIdAsync_Item_Does_Not_Exist()
        {
            _sut = new ToDoService(_todoRepo.Object);
        }

        protected override void Given()
        {
            base.Given();
            ToDo todo = null;
            _todoRepo.Setup(x => x.GetAsync(_todoId)).Returns(Task.FromResult(todo));
        }

        protected override void When()
        {
            base.When();
            _todoItem = _sut.GetAsync(_todoId).Result;
        }

        [Test]
        public void Then_The_Todo_Repo_Is_Called_Once()
        {
            _todoRepo.Verify(x => x.GetAsync(_todoId), Times.Once);
            _todoRepo.VerifyNoOtherCalls();
        }

        [Test]
        public void Then_A_Null_Object_Is_Returned()
        {
            Assert.IsNull(_todoItem);
        }
    }
}
