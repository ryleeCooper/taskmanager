﻿using Moq;
using NUnit.Framework;
using SpecsFor.StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskManager.Common.Contracts.ToDo;
using TaskManager.Data.Models;
using TaskManager.Data.Repositories;
using TaskManager.Services.Services;

namespace TaskManager.Tests.ServiceTests.TodoService
{
    [TestFixture]
    public class GetAsync_List_Has_No_Items : SpecsFor<ToDoService>
    {
        private readonly IToDoService _sut;
        private readonly Mock<IToDoRepository> _todoRepo = new Mock<IToDoRepository>();
        private IEnumerable<ToDoDto> _todos;

        public GetAsync_List_Has_No_Items()
        {
            _sut = new ToDoService(_todoRepo.Object);
        }

        protected override void Given()
        {
            base.Given();
            IEnumerable<ToDo> todos = new List<ToDo>();

            _todoRepo.Setup(x => x.GetAsync()).Returns(Task.FromResult(todos));
        }

        protected override void When()
        {
            base.When();
            _todos = _sut.GetAsync().Result;
        }

        [Test]
        public void Then_The_Todo_Repo_Is_Called_Once()
        {
            _todoRepo.Verify(x => x.GetAsync(), Times.Once);
            _todoRepo.VerifyNoOtherCalls();
        }

        [Test]
        public void Then_An_IEnumerable_Is_Returned()
        {
            Assert.NotNull(_todos);
        }

        [Test]
        public void Then_IEnumerable_Has_No_Items()
        {
            Assert.AreEqual(_todos.Count(), 0);
        }
    }
}
