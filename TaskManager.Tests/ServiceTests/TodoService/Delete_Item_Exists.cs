﻿using Moq;
using NUnit.Framework;
using TaskManager.Data.Repositories;
using SpecsFor.Core;
using TaskManager.Services.Services;
using System.Threading.Tasks;
using System;
using SpecsFor.StructureMap;

namespace TaskManager.Tests.ServiceTests.TodoService
{
    [TestFixture]
    public class Delete_Item_Exists : SpecsFor<ToDoService>
    {
        private readonly ToDoService _sut;
        private readonly Mock<IToDoRepository> _todoRepo = new Mock<IToDoRepository>();
        private int _todoId = 0;
        private bool _deleteSuccess;

        public Delete_Item_Exists()
        {
            _sut = new ToDoService(_todoRepo.Object);
        }

        protected override void Given()
        {
            base.Given();
            var rng = new Random();
            _todoId = rng.Next(1, 100);

            _todoRepo.Setup(x => x.DeleteAsync(_todoId)).Returns(Task.FromResult(true));
        }

        protected override void When()
        {
            base.When();
            _deleteSuccess = _sut.DeleteAsync(_todoId).Result;
        }

        [Test]
        public void Then_The_Todo_Repo_Is_Called_Once()
        {
            _todoRepo.Verify(x => x.DeleteAsync(_todoId), Times.Once);
            _todoRepo.VerifyNoOtherCalls();
        }

        [Test]
        public void Then_Delete_Is_Successful()
        {
            Assert.AreEqual(_deleteSuccess, true);
        }
    }
}
