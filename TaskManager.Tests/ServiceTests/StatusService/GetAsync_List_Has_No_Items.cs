﻿using Moq;
using NUnit.Framework;
using SpecsFor.StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskManager.Common.Contracts.Status;
using TaskManager.Data.Models;
using TaskManager.Data.Repositories;
using TaskManager.Services.Services;

namespace TaskManager.Tests.ServiceTests.StatusService
{
    [TestFixture]
    public class GetAsync_List_Has_No_Items : SpecsFor<Services.Services.StatusService>
    {
        private readonly IStatusService _sut;
        private readonly Mock<IStatusRepository> _statusRepo = new Mock<IStatusRepository>();
        private int _statusId = 0;
        private IEnumerable<StatusDto> _statuses;

        public GetAsync_List_Has_No_Items()
        {
            _sut = new Services.Services.StatusService(_statusRepo.Object);
        }

        protected override void Given()
        {
            base.Given();
            var rng = new Random();
            _statusId = rng.Next(1, 100);

            IEnumerable<Status> statuses = new List<Status>();

            _statusRepo.Setup(x => x.GetAsync()).Returns(Task.FromResult(statuses));
        }

        protected override void When()
        {
            base.When();
            _statuses = _sut.GetAsync().Result;
        }

        [Test]
        public void Then_The_Status_Repo_Is_Called_Once()
        {
            _statusRepo.Verify(x => x.GetAsync(), Times.Once);
            _statusRepo.VerifyNoOtherCalls();
        }

        [Test]
        public void Then_An_IEnumerable_Is_Returned()
        {
            Assert.NotNull(_statuses);
        }

        [Test]
        public void Then_IEnumerable_Has_Items()
        {
            Assert.AreEqual(_statuses.Count(), 0);
        }
    }
}
