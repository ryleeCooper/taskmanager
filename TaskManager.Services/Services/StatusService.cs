﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TaskManager.Common.Contracts.Status;
using TaskManager.Data.Repositories;
using TaskManager.Services.Mapping;

namespace TaskManager.Services.Services
{
    public class StatusService : IStatusService
    {
        private readonly IStatusRepository _statusRepository;

        public StatusService(IStatusRepository statusRepository)
        {
            _statusRepository = statusRepository;
        }

        public async Task<IEnumerable<StatusDto>> GetAsync()
        {
            var statuses = await _statusRepository.GetAsync();

            return statuses.Select(StatusMapper.DomainToDto).ToList();
        }
    }
}
