﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TaskManager.Common.Contracts.Status;

namespace TaskManager.Services.Services
{
    public interface IStatusService
    {
        Task<IEnumerable<StatusDto>> GetAsync();
    }
}
