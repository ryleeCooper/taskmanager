﻿using System;
using System.Collections.Generic;
using System.Text;
using TaskManager.Common.Contracts.Status;
using TaskManager.Data.Models;

namespace TaskManager.Services.Mapping
{
    public class StatusMapper
    {
        public static StatusDto DomainToDto(Status domain) => domain != null
            ? new StatusDto
            {
                Id = domain.Id,
                Name = domain.Name
            }
            : null;

        public static Status DtoToDomain(StatusDto dto) => new Status
        {
            Id = dto.Id,
            Name = dto.Name
        };
    }
}
