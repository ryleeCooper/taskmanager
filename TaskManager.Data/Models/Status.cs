﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManager.Data.Models
{
    public class Status
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<ToDo> ToDos { get; set; }
    }
}
