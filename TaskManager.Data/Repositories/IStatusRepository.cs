﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TaskManager.Data.Models;

namespace TaskManager.Data.Repositories
{
    public interface IStatusRepository
    {
        Task<IEnumerable<Status>> GetAsync();
    }
}
