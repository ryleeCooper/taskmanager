﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TaskManager.Data.Migrations
{
    public partial class StatusToDoRelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "StatusId",
                table: "ToDo",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Statuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Statuses", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ToDo_StatusId",
                table: "ToDo",
                column: "StatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_ToDo_Statuses_StatusId",
                table: "ToDo",
                column: "StatusId",
                principalTable: "Statuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.Sql("SET IDENTITY_INSERT [dbo].[Statuses] ON");
            migrationBuilder.Sql("INSERT [dbo].[Statuses] ([Id], [Name]) VALUES (1, N'To Do'), (2, N'In Progress'), (3, N'Done')");
            migrationBuilder.Sql("SET IDENTITY_INSERT [dbo].[Statuses] OFF");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ToDo_Statuses_StatusId",
                table: "ToDo");

            migrationBuilder.DropTable(
                name: "Statuses");

            migrationBuilder.DropIndex(
                name: "IX_ToDo_StatusId",
                table: "ToDo");

            migrationBuilder.DropColumn(
                name: "StatusId",
                table: "ToDo");
        }
    }
}
