﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.Common.Contracts.Status;
using TaskManager.Web.Models;

namespace TaskManager.Web.Mapping
{
    public class StatusMapper
    {
        public static StatusDto WebToDto(Status web) => web != null
            ? new StatusDto
            {
                Id = web.Id,
                Name = web.Name
            }
            : null;

        public static Status DtoToWeb(StatusDto dto) => new Status
        {
            Id = dto.Id,
            Name = dto.Name
        };
    }
}
