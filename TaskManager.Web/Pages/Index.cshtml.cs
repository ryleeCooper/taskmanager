﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using TaskManager.Web.Models;
using TaskManager.Web.Services;

namespace TaskManager.Web.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IToDoService _toDoService;
        private readonly IStatusService _statusService;
        private readonly IConfiguration _configuration;

        public IndexModel(IToDoService toDoService, IStatusService statusService, IConfiguration configuration)
        {
            _toDoService = toDoService;
            _statusService = statusService;
            _configuration = configuration;
        }

        public List<ToDo> ToDos { get; set; }
        public List<SelectListItem> Statuses { get; set; }
        public ToDoProgress ToDoProgress
        {
            get
            {
                var allCount = Convert.ToDouble(ToDos.Count());
             
                var pastDueCount = ToDos.Where(x => x.DueDateIndicatorClass == "past-due").Count();
                var upcomingCount = ToDos.Where(x => x.DueDateIndicatorClass == "upcoming").Count();
                var onTargetCount = ToDos.Where(x => x.DueDateIndicatorClass == "on-target").Count();
                return new ToDoProgress()
                {
                    OnTargetPercent = (int)(Math.Round((onTargetCount / allCount) * 100, 0, MidpointRounding.AwayFromZero)),
                    UpcomingPercent = (int)(Math.Round((upcomingCount / allCount) * 100, 0, MidpointRounding.AwayFromZero)),
                    PastDuePercent = (int)(Math.Round((pastDueCount / allCount) * 100, 0, MidpointRounding.AwayFromZero))
                };
            }
        }
        public string ApiUriBase { get; set; }

        public async Task OnGetAsync()
        {
            try
            {
                ToDos = (await _toDoService.GetAsync()).ToList();
                Statuses = (await _statusService.GetSelectListAsync()).ToList();
                ApiUriBase = _configuration["ApiUriBase"];
            }
            catch (HttpRequestException)
            {
                ToDos = new List<ToDo>();
            }
        }
    }
}
