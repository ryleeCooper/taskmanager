﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.Web.Models;
using TaskManager.Web.Services;

namespace TaskManager.Web.Pages
{
    public class EditModel : PageModel
    {
        private readonly IToDoService _toDoService;
        private readonly IStatusService _statusService;

        public EditModel(IToDoService toDoService, IStatusService statusService)
        {
            _toDoService = toDoService;
            _statusService = statusService;
        }

        [BindProperty]
        public ToDo ToDo { get; set; }
        public List<SelectListItem> Statuses { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ToDo = await _toDoService.GetAsync(id.Value);
            Statuses = (await _statusService.GetSelectListAsync()).ToList();

            if (ToDo == null)
            {
                return NotFound();
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var isSuccess = await _toDoService.PutAsync(ToDo);

            return isSuccess ? RedirectToPage("./Index") : RedirectToPage("./Error");
        }
    }
}
