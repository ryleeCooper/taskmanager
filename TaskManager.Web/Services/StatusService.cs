﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using TaskManager.Common.Contracts.Status;
using TaskManager.Web.Mapping;
using TaskManager.Web.Models;

namespace TaskManager.Web.Services
{
    public class StatusService : IStatusService
    {
        public HttpClient Client { get; }

        public StatusService(HttpClient client)
        {
            client.BaseAddress = new Uri("https://localhost:44318/");
            client.DefaultRequestHeaders.Add("Accept", "application/json");

            Client = client;
        }
        public async Task<IEnumerable<Status>> GetAsync()
        {
            var response = await Client.GetAsync("/api/Statuses");
            response.EnsureSuccessStatusCode();

            using var responseStream = await response.Content.ReadAsStreamAsync();
            var dtos = await JsonSerializer.DeserializeAsync<List<StatusDto>>(responseStream);

            return dtos.Select(dto => StatusMapper.DtoToWeb(dto));
        }

        public async Task<IEnumerable<SelectListItem>> GetSelectListAsync()
        {
            var statuses = await GetAsync();

            return statuses.Select(status => new SelectListItem(status.Name, status.Id.ToString()));
        }
    }
}
