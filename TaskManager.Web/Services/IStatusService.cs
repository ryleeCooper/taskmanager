﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.Web.Models;

namespace TaskManager.Web.Services
{
    public interface IStatusService
    {
        Task<IEnumerable<Status>> GetAsync();
        Task<IEnumerable<SelectListItem>> GetSelectListAsync();
    }
}
