﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TaskManager.Web.Models
{
    public class ToDo
    {
        public int Id { get; set; }
        [Display(Name = "Title")]
        public string Title { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Due Date")]
        public DateTime DueDate { get; set; }
        [Display(Name = "Notes")]
        public string Notes { get; set; }
        public int StatusId { get; set; }
        public string StatusName => StatusId switch
        {
            1 => "To Do",
            2 => "In Progress",
            3 => "Done",
            _ => string.Empty,
        };
        public string StatusClass => StatusId switch
        {
            1 => "to-do-status",
            2 => "in-progress-status",
            3 => "done-status",
            _ => string.Empty,
        };
        public string DueDateIndicatorClass => DueDate < DateTime.Now ? "past-due"
            : DueDate <= DateTime.Now.AddDays(30) ? "upcoming"
            : "on-target";
    }
}
