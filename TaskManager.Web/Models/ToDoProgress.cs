﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManager.Web.Models
{
    public class ToDoProgress
    {
        public int PastDuePercent { get; set; }
        public int UpcomingPercent { get; set; }
        public int OnTargetPercent { get; set; }
    }
}
