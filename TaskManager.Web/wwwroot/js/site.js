﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your Javascript code.
$(document).ready(function () {
    $('.status-dropdown').change(function () {
        SetNewStatusClass($(this).data('id'), $(this).val());
        if (!$(this).hasClass('edit-status')) {
            UpdateToDoStatus($(this).data('id'), $(this).val());
        }
    });
});

function UpdateToDoStatus(todoId, statusId) { 
    var oldStatus = $(`#todo-${todoId}`).data('status');
    var toDo = {
        id: $(`#todo-${todoId}`).data('id'),
        title: $(`#todo-${todoId}`).data('title'),
        description: $(`#todo-${todoId}`).data('description'),
        duedate: new Date($(`#todo-${todoId}`).data('duedate')).toISOString(),
        notes: $(`#todo-${todoId}`).data('notes'),
        statusid : parseInt(statusId)
    };

    $.ajax({
        url: `${_apiUri}/api/ToDos/${todoId}`,
        type: 'PUT',
        async: true,
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(toDo),
        xhrFields: {
            withCredentials: true
        }
    }).done(function (data) {
        $(`#todo-${todoId}`).data('status', statusId);
        Swal.fire({
            icon: 'success',
            title: 'Saved!',
            showConfirmButton: false,
            timer: 1500
        });
    }).fail(function (error) {
        $(`#select-${todoId}`).val(oldStatus);
        SetNewStatusClass(todoId, oldStatus.toString());
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
            showConfirmButton: false,
            timer: 1500
        });
    });
}

function SetNewStatusClass(todoId, newStatusId) {
    $(`#select-${todoId}`).removeClass("to-do-status in-progress-status done-status");

    switch (newStatusId) {
        case "1":
            $(`#select-${todoId}`).addClass("to-do-status");
            break;
        case "2":
            $(`#select-${todoId}`).addClass("in-progress-status");
            break;
        case "3":
            $(`#select-${todoId}`).addClass("done-status");
            break;
        default:
            break;
    }
}
